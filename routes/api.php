<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

## Prefijo http://miruta.com/api/{método}


# http://workapi.test/api/user?api_token=XXXXXXXXXXX

//Route::middleware('auth:api')->get('/user', function (Request $request) {
Route::middleware('auth:api')->group(function() {
  Route::get('/products/all', 'ProductsController@index')->name('products.all');
  Route::get('/correo/basico', 'MailController@enviarBasico')->name('correo.basico');
  Route::get('/correo/html', 'MailController@enviarHtml')->name('correo.html');
  Route::get('/correo/template', 'MailController@enviarTemplate')->name('correo.template');

  #http://workapi.test/api/{archivoLocal}?api_token=XXXXXXXXXX
  #Subir archivos locales
  Route::post('/archivoLocal', 'ProductsController@storeLocal')->name('products.store');

  #http://workapi.test/api/{archivoAmazon}?api_token=XXXXXXXXXX
  #Subir archivos Amazon
  Route::post('/archivoAmazon', 'ProductsController@storeAmazon')->name('products.store');

  #Twitter
  #http://workapi.test/api/tw/timeline?api_token=1ca9d38a401e8ad6dd8453acc40bfa
  #Route::get('/tw/timeline', 'TwitterController@timeline')->name('timeline');

  #http://workapi.test/api/tw/timeline/5/screen_name?api_token=1ca9d38a401e8ad6dd8453acc40bfa
  Route::get('/tw/timeline/{count}/{screenName}', 'TwitterController@timeline')->name('timeline');

  Route::get('/tw/search/{count}/{search}', 'TwitterController@search')->name('search');


});

#GET - Petición
Route::get('welcome', function(){
    return response()->json(['data' => 'Bienvenidos a Workshop de Laravel', 'code' => 200]);
});












