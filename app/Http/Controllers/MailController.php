<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
#use App\Mail\TestEmail;

class MailController extends Controller
{
    //Enviar correos con una estructura básica
    public function enviarBasico() {
        $data = ['mensaje' => 'Bienvenidos al Workshop'];
        #$data = array('mensaje' => 'Bienvenidos al Workshop');

        Mail::send([], $data, function($body) {
            //Cuerpo del envío del correo electrónico
            //To - Para quien
            //From - Desde donde
            $body->to('sepalaball@gmail.com', 'Yesi Days')->subject('Laravel');
            $body->from('sepalaball@gmail.com', 'Edteam');
        });

        return response()->json([
            'response' => 'Se envió correctamente el correo',
            'code'     => 200
        ]);

    }

    //Enviar correos con datos y html
    public function enviarHtml() {
        $data = [
            'titulo' => 'Bienvenidos al Workshop',
            'mensaje' => 'Vestibulum vel ante purus. Morbi viverra magna tellus, ac dictum nisi condimentum sit amet. Fusce a porta ante, dapibus aliquam justo. Mauris sit amet imperdiet lectus.'
        ];

        Mail::send('mails.testing', $data, function($body) use ($data){
            $body->to('sepalaball@gmail.com', 'Yesi Days');
            $body->subject('Nuevos cursos ' .$data['titulo']);
            $body->from('sepalaball@gmail.com', 'Edteam');
        });

        return response()->json([
            'response' => 'Se envió correctamente el correo',
            'code'     => 200
        ]);

    }

    //Enviar correos con datos y una plantilla
    public function enviarTemplate() {
        $data = [
            'titulo' => 'Bienvenidos al Workshop',
            'mensaje' => 'Vestibulum vel ante purus. Morbi viverra magna tellus, ac dictum nisi condimentum sit amet. Fusce a porta ante, dapibus aliquam justo. Mauris sit amet imperdiet lectus.'
        ];

        Mail::send('mails.template', $data, function($body) use ($data){
            $body->to('sepalaball@gmail.com', 'Yesi Days');
            $body->subject('Nuevos cursos ' .$data['titulo']);
            $body->from('sepalaball@gmail.com', 'Edteam');
        });

        return response()->json([
            'response' => 'Se envió correctamente el correo con un template',
            'code'     => 200
        ]);

    }

}
